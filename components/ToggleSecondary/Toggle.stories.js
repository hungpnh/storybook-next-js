import React from 'react';
import ToggleSecondary from './ToggleSecondary';

export default {
  title: 'MangoAds/Atoms/Form/Toggle',
  component: ToggleSecondary,
  argTypes: {
    onChange: { action: " change " }
  }
}

const Template = (args) => <ToggleSecondary {...args} />

export const Secondary = Template.bind({})
Secondary.args = {
  className: "",
  message: "Toggle ME",
  checked: false,
  disabled: false
}

