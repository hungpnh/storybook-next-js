import React, { useEffect, useState } from 'react'

const ToggleSecondary = ({ message, onChange, checked, disabled }) => {
  const [check, setCheck] = useState(false)
  useEffect(() => {
    setCheck(checked)
  }, [checked])
  const handleOnChange = (e) => {
    onChange(e)
    setCheck(!check)
  }


  return <div disabled={disabled} className={`toggle ${disabled ? "disabled-toggle" : ""}`}>
    <label htmlFor="toggleB" className="toggle-label">
      <div className="input-wrapper">

        <input disabled={disabled} checked={check} onChange={handleOnChange} type="checkbox" id="toggleB" className="sr-only toggle-input" />

        <div className="toggle-divb"></div>

        <div className="toggle-dotb"></div>
      </div>

      <div className="toggle-message">
        {message}
      </div>
    </label>

  </div>
}

export default ToggleSecondary;