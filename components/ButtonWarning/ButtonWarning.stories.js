import React from 'react';
import ButtonWarning from './ButtonWarning';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonWarning,
  argTypes: { onClick: { action: 'clicked' } },
}

const Template = (args) => <ButtonWarning {...args}/>

export const Warning = Template.bind({})
Warning.args = {
  className: 'button-warning',
  message: 'Button Warning',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false
}

