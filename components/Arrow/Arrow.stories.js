import React from 'react';
import Arrow from './Arrow';

export default {
  title: 'MangoAds/Atoms/Arrow',
  component: Arrow,
  argTypes: {
    onClick: { action: "clicked" },
    type: {
      control: {
        type: 'select',
        options: ['up', 'down', 'right', 'left']
      }
    }
  }

}

const Template = (args) => <Arrow {...args} />

export const Primary = Template.bind({})
Primary.args = {
  type: {},
  className: "",
  disabled: false,

}


