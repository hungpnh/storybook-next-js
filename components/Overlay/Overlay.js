import React from 'react'

const Overlay = ({ className }) => {
  return(
    <div className={className?.length != 0 ? className : 'overlay-primary'}></div>
  )
}

export default Overlay;