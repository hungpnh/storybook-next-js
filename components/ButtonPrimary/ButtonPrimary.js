import React from 'react'
import Icon from '../Icon'

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}
const ButtonPrimary = ({ className="button-primary",onClick, disabled, buttonStyle, message, iconName, reverse}) => {
  return (
    <button 
      onClick={onClick} 
      className={
        classNames(className?.length != 0 ? className : 'button-primary', 
        disabled ? 'button-disabled' : '')}  
      disabled={disabled} 
      style={buttonStyle}
    >
      {message}
      {iconName ? 
        <Icon
          className={`icon-white ${reverse ? 'float-left' : 'float-right'}`}
          name={iconName}
          optionsType="outline"
        />
      : ''}
    </button>
 )
}

export default ButtonPrimary;