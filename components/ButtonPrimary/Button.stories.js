import React from 'react';
import ButtonPrimary from './ButtonPrimary';
import { action } from '@storybook/addon-actions';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonPrimary,
  argTypes: { 
    onClick: { 
      action: 'clicked' 
    },
    className: {
      control: {
        type: 'select',
        options: ['button-primary', 'button-secondary', 'button-success', 'button-warning', 'button-white', 'button-dark', 'button-danger', 'button-info']
      },
      defaultValue: 'button-primary'
    }
  },
}

const Template = (args) => <ButtonPrimary {...args} />

const testFunction = (e) => {
  console.log(e);
}

export const Primary = Template.bind({})
Primary.args = {
  message: 'Button Primary',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false
}


