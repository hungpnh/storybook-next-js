import React from 'react'
const PriceRegular = ({ className, regular }) => {
  return (
    <span
      type="submit"
      className={className?.length != 0 ? className : 'price-regular'}
    >
      ${regular}
    </span>
  )
}

export default PriceRegular;