import React from 'react';
import Heading5 from './Heading5';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading5,
}

const Template = (args) => <Heading5 {...args} />

export const H5 = Template.bind({})
H5.args = {
  message: 'h5 display 5',
  className: ""
}

