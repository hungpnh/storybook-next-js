import React from 'react'

const Heading5 = ({ message, className }) => {
    return <h1 className={className ? className : "heading5"}>
        {message}
    </h1>
}

export default Heading5;