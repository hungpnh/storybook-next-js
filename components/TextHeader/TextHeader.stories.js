import React from 'react';
import TextHeader from './TextHeader';
import { action } from '@storybook/addon-actions';

export default {
  title: 'MangoAds/Atoms/Text',
  component: TextHeader,
}

const Template = (args) => <TextHeader {...args} />

export const Header = Template.bind({})
Header.args = {
  className: 'text-header',
  children: 'this is paragraph'
}


