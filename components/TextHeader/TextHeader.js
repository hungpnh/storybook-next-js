import React from 'react'

const TextHeader = ({ children, className }) => {
  return (
    <h3 className= {className ? className : ''}>
      {children}
    </h3>
  )
}

export default TextHeader;