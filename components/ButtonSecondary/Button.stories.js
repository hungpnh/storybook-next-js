import React from 'react';
import ButtonSecondary from './ButtonSecondary';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonSecondary,
  argTypes: { onClick: { action: 'clicked' } },
}

const Template = (args) => <ButtonSecondary {...args}/>

export const Secondary = Template.bind({})
Secondary.args = {
  className: 'button-secondary',
  message: 'Button Secondary',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false
}

