import React from 'react'
import Icon from '../Icon'
const LinkIcon = ({ className,link, title, colorIcon, nameIcon }) => {
  return(
    <a 
    className={className?.length != 0 ? className : 'link-primary'}
      href={link}
      title={title}
    >
      <Icon
        className={colorIcon}
        name={nameIcon}
        optionsType="outline"
      />
      Redirect to home page
    </a>
  )
}

export default LinkIcon;