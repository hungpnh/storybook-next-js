import React from 'react';
import Arrow from './Arrow';

export default {
  title: 'MangoAds/Atoms/Arrow',
  component: Arrow,
  argTypes: {
    onClick: { action: "clicked" },
    type: {
      control: {
        type: 'select',
        options: ['up', 'down', 'right', 'left']
      },
    }
  }
}

const Template = (args) => <Arrow  {...args} />

export const Rounded = Template.bind({})
Rounded.args = {
  type: {},
  className: "",
  disabled: false,

}

