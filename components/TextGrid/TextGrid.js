import React from 'react'
import { CalendarIcon, CheckIcon, PlusIcon, UsersIcon, ViewBoardsIcon, ViewListIcon } from '@heroicons/react/outline'
import Icon from '../Icon'


const TextGrid = ({colorIcon, nameIcon, className, cols ,data}) => {
  return (
    <ul className={`rounded overflow-hidden grid gap-px sm:grid-cols-${cols}`}>
              {data.map((item) => (
                <li
                  key={item}
                  className="text-grid"
                >
                  <Icon
                    className={colorIcon}
                    name={nameIcon}
                    optionsType="outline"
                  />
                  <span>{item}</span>
                </li>
              ))}
    </ul>
  )
}

export default TextGrid;