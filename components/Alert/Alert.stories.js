import React from 'react';
import Alert from './Alert';

export default {
    title: 'MangoAds/Atoms/Alert',
    component: Alert,
}

const Template = (args) => <Alert {...args} />

export const AlertPrimary = Template.bind({})
AlertPrimary.args = {
    title: "Title Alert",
    open: true,
    message: "message alert",
    className: "",
}


