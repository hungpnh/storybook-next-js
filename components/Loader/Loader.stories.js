import React from 'react';
import Loader from './Loader';

export default {
  title: 'MangoAds/Atoms/Loader',
  component: Loader,
  argTypes: {
    className: {
      control: {
        type: 'select',
        options: ['loader-spin', 'loader-ping']
      },
      defaultValue: 'loader-spin'
    }
  }
}

const Template = (args) => <Loader {...args}/>

export const Spin = Template.bind({})
Spin.args = {
    type: 'primary',
    loaderStyle: {}
}
