import React from 'react';
import AlertDanger from './AlertDanger';

export default {
    title: 'MangoAds/Atoms/Alert',
    component: AlertDanger,
}

const Template = (args) => <AlertDanger {...args} />

export const Danger = Template.bind({})
Danger.args = {
    title: "Title Alert",
    open: true,
    message: "message alert",
    className: "",

}

