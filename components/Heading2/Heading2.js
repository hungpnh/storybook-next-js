import React from 'react'

const Heading2 = ({ message, className }) => {
    return <h1 className={className ? className : "heading2"}>
        {message}
    </h1>
}

export default Heading2;