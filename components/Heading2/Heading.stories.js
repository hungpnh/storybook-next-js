import React from 'react';
import Heading2 from './Heading2';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading2,
}

const Template = (args) => <Heading2 {...args} />

export const H2 = Template.bind({})
H2.args = {
  message: 'h2 display 2',
  className: ""
}

