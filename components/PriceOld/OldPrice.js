import React from 'react'
const OldPrice = ({ className, priceOld }) => {
  return (
    <span
    className={className?.length != 0 ? className : 'price-regular'}
    >
      ${priceOld}
    </span>
  )
}

export default OldPrice;