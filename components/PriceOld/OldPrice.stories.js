import React from 'react';
import OldPrice from './OldPrice';

export default {
  title: 'MangoAds/Atoms/Price',
  component: OldPrice,
  argTypes: {
    special: {control: {type: 'number',min: 0}, table: {category: 'Price'}},
  }
}

const Template = (args) => <OldPrice {...args}/>

export const Old = Template.bind({})
Old.args = {
    className: 'price-special',
    priceOld: 10000,
    priceStyle: {}
}

