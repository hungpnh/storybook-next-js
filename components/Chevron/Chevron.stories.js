import React from 'react';
import Chevron from './Chevron';

export default {
  title: 'MangoAds/Atoms/Chevron',
  component: Chevron,
  argTypes: {
    type: {
      control: {
        type: 'select',
        options: ['up', 'down', 'right', 'left']
      }
    }
  }
}

const Template = (args) => <Chevron {...args} />

export const Primary = Template.bind({})
Primary.args = {
  className: "",
  type
}

