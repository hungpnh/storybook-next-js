import React from 'react'

const Chevron = ({ type, className }) => {
  let ar = <svg xmlns="http://www.w3.org/2000/svg" className={className ? className : "chevron"} fill="none" viewBox="0 0 24 24" stroke="currentColor">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 14l-7 7m0 0l-7-7m7 7V3" />
  </svg>
  switch (type) {
    case "up":
      ar = <svg xmlns="http://www.w3.org/2000/svg" className={className ? className : "chevron"} fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 15l7-7 7 7" />
      </svg>
      break;
    case "down":
      ar = <svg xmlns="http://www.w3.org/2000/svg" className={className ? className : "chevron"} fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
      </svg>
      break;
    case "right":
      ar = <svg xmlns="http://www.w3.org/2000/svg" className={className ? className : "chevron"} fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
      </svg>
      break;
    case "left":
      ar = <svg xmlns="http://www.w3.org/2000/svg" className={className ? className : "chevron"} fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7" />
      </svg>
      break;

    default:
      ar = <svg xmlns="http://www.w3.org/2000/svg" className={className ? className : "chevron"} fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
      </svg>
      break;
  }



  return <>{ar}</>
}

export default Chevron;