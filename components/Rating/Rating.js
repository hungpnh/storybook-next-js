import React from 'react'
import { StarIcon as StarIconOl } from '@heroicons/react/outline'
import { StarIcon as StarIconSo } from '@heroicons/react/solid'
import Icon from '../Icon'
const Rating = ({ max, score, className, colorIcon }) => {
  const listRating = []
  for (let index = 1; index <= max; index++) {
    listRating.push(
      <Icon
        className="icon-warning"
        name="StarIcon"
        optionsType={(index <= score) ? 'solid' : 'outline'}
      />
    )
  }
  return (
    <div className={className?.length != 0 ? className : 'rating-primary'}>
      {listRating.map((value, index) => <div key={index}>{value}</div>)}
    </div>
  )
}

export default Rating;