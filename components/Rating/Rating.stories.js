import React from 'react';
import Rating from './Rating';

export default {
  title: 'MangoAds/Atoms/Rating',
  component: Rating,
  argTypes: {
    max: {control: {type: 'number',min: 0, max: 5}},
    score: {control: {type: 'number',min: 0, max: 5}},
  }
}

const Template = (args) => <Rating {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    className: 'rating-primary',
    max: 5,
    score: 4,
    ratingStyle: {}
}

