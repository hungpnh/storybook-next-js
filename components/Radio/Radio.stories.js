import React from 'react';
import Radio from './Radio';

export default {
  title: 'MangoAds/Atoms/Form/Radio',
  component: Radio,
  argTypes: {
    onChange: { action: "change" }
  }
}

const Template = (args) => <Radio {...args} />

export const Primary = Template.bind({})
Primary.args = {
  label: "",
  value: "",
  checked: false,
  name: "",
  className: "",

  disabled: false
}

