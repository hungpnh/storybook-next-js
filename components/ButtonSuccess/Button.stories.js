import React from 'react';
import ButtonSuccess from './ButtonSuccess';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonSuccess,
  argTypes: { onClick: { action: 'clicked' } },
}

const Template = (args) => <ButtonSuccess {...args}/>

export const Success = Template.bind({})
Success.args = {
  className: 'button-success',
  message: 'Button Success',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false
}

