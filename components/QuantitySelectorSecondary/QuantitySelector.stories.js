import React from 'react';
import QuantitySelectorSecondary from './QuantitySelectorSecondary';

export default {
  title: 'MangoAds/Atoms/QuantitySelector',
  component: QuantitySelectorSecondary,
}

const Template = (args) => <QuantitySelectorSecondary {...args} />

export const Secondary = Template.bind({})
Secondary.args = {
  disabledDecrease: false,
  disabledIncrease: false,
  disabledLabel: false,
  className: ""
}

