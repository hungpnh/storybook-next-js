import React, { useState } from 'react'

const QuantitySelectorSecondary = ({ disabledIncrease, disabledDecrease, disabledLabel, className }) => {
  const [quantity, setQuantity] = useState(1)
  const handleClickDecrease = () => {
    setQuantity(+quantity - 1)
  }
  const handleClickIncrease = () => {

    setQuantity(+quantity + 1);
  }
  const handleOnChange = (e) => {
    if (!isNaN(e.target.value)) {
      setQuantity(e.target.value)
    }
  }
  return <div className="quantity-secondary">
    <button disabled={disabledDecrease} onClick={handleClickDecrease}>-</button>

    <input disabled={disabledLabel} size="1" onChange={handleOnChange} type="text" value={quantity} />

    <button disabled={disabledIncrease} onClick={handleClickIncrease}>+</button>
  </div>
}

export default QuantitySelectorSecondary;