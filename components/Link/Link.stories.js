import React from 'react';
import Link from './Link';

export default {
  title: 'MangoAds/Atoms/Link',
  component: Link,
}

const Template = (args) => <Link {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    className: 'link-primary',
    message: 'Check this out!',
    link: 'https://mangoads.vn/',
    title: "Go to home page of MangoAds Company",
    linkStyle: {}
}

