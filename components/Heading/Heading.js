import React from 'react'

const Heading = ({ message, className }) => {
  return <h1 className={className ? className : "heading1"}>
    {message}
  </h1>
}

export default Heading;