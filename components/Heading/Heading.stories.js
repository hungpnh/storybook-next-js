import React from 'react';
import Heading from './Heading';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading,
}

const Template = (args) => <Heading {...args} />

export const H1 = Template.bind({})
H1.args = {
  message: 'h1 display 1',
  className: ""
}

