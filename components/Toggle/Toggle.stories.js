import React from 'react';
import Toggle from './Toggle';

export default {
  title: 'MangoAds/Atoms/Form/Toggle',
  component: Toggle,
  argTypes: {
    onChange: { action: " change " }
  }
}

const Template = (args) => <Toggle {...args} />

export const Primary = Template.bind({})
Primary.args = {
  className: "",
  message: "Toggle ME",
  disabled: false,
  checked: false
}

