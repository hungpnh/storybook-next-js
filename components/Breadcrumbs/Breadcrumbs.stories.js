import React from 'react';
import Breadcrumbs from './Breadcrumbs';

export default {
  title: 'MangoAds/Atoms/Breadcrumbs',
  component: Breadcrumbs,
  argTypes: {
    onClick: { action: "clicked" },
  }

}

const Template = (args) => <Breadcrumbs {...args} />

export const Primary = Template.bind({})
Primary.args = {
  active: false,
  message: "Home",
  className: "",
  href: ""
}

