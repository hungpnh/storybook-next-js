import React from 'react'

const Image = ({ src = "", alt = "", width, className }) => {
  return <img loading="lazy" className={className ? className : ""} src={src} alt={alt} width={width} />
}

export default Image;