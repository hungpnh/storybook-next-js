import React from 'react';
import CircleIconWarning from './CircleIconWarning';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/CircleIcon',
  component: CircleIconWarning,
  argTypes: {
    optionsType: {
      control: {
        type: 'inline-radio',
        options: ['solid', 'outline'],
      },
      table: { category: 'Options' },
      defaultValue: "outline"
    },
    onClick: { action: 'clicked' },
  }
}

const Template = (args) => <CircleIconWarning {...args}/>

export const Warning = Template.bind({})
Warning.args = {
    className: 'circle-icon-warning',
    nameIcon: 'HomeIcon',
}

