import React from 'react';
import CircleIconDisabled from './CircleIconDisabled';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/CircleIcon',
  component: CircleIconDisabled,
  argTypes: {
    optionsType: {
      control: {
        type: 'inline-radio',
        options: ['solid', 'outline'],
      },
      table: { category: 'Options' },
      defaultValue: "outline"
    },
    colorIcon: {
      control: {
        type: 'select',
        options: ['icon-primary', 'icon-warning', 'icon-danger', 'icon-secondary', 'icon-success', 'icon-white', 'icon-black']
      },
      defaultValue: 'icon-primary'
    },
    onClick: { action: 'clicked' },
  }
}

const Template = (args) => <CircleIconDisabled {...args}/>

export const Disabled = Template.bind({})
Disabled.args = {
    className: 'circle-icon-disabled',
    nameIcon: 'HomeIcon',
}

