import React from 'react'

const Heading3 = ({ message, className }) => {
    return <h1 className={className ? className : "heading3"}>
        {message}
    </h1>
}

export default Heading3;