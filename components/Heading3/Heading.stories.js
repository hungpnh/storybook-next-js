import React from 'react';
import Heading3 from './Heading3';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading3,
}

const Template = (args) => <Heading3 {...args} />

export const H3 = Template.bind({})
H3.args = {
  message: 'h3 display 3',
  className: ""
}

