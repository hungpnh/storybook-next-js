import React from 'react';
import TextArea from './TextArea';
import { action } from '@storybook/addon-actions';

export default {
  title: 'MangoAds/Atoms/TextArea',
  component: TextArea,
  argTypes: {
    placeholder: {table: {category: 'Text'}},
    textAreaStyle: {table: {category: 'UI'}},
    className: {table: {category: 'UI'}}
  }
}

const Template = (args) => <TextArea {...args} />

export const Primary = Template.bind({})
Primary.args = {
  className: 'textarea-primary',
  placeholder: 'Enter something here...',
  disabled: false,
  rows: 8,
  textAreaStyle: {},
}


