import React from 'react'

const TextArea = ({ className, rows, textAreaStyle, placeholder, disabled }) => {
  return (
    <textarea
      rows={rows}
      className={className?.length != 0 ? className : 'textarea-primary'} 
      disabled={disabled}
      placeholder={placeholder}
      style = {textAreaStyle}
    />
  )
}

export default TextArea;