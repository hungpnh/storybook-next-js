import React from 'react';
import CheckBox from './CheckBox';

export default {
  title: 'MangoAds/Atoms/Form/CheckBox',
  component: CheckBox,
  argTypes: {
    onChange: { action: "onChange" }
  }
}

const Template = (args) => <CheckBox {...args} />

export const Primary = Template.bind({})
Primary.args = {
  label: "",
  value: "",
  checked: false,
  name: "",
  className: "",
  disabled: false
}

