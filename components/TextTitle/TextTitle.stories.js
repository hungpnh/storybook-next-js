import React from 'react';
import TextTitle from './TextTitle';
import { action } from '@storybook/addon-actions';

export default {
  title: 'MangoAds/Atoms/Text',
  component: TextTitle,
}

const Template = (args) => <TextTitle {...args} />

export const Title = Template.bind({})
Title.args = {
  className: 'text-title',
  children: 'this is paragraph'
}


