import React from 'react';
import TextList from './TextList';
import { action } from '@storybook/addon-actions';
const listText = [
  'Vitae in pulvinar odio id utobortis in inter.',
  'Sed sed id viverra viverra augue eget massa.',
]
export default {
  title: 'MangoAds/Atoms/Text',
  component: TextList,
}

const Template = (args) => <TextList {...args} />

export const List = Template.bind({})
List.args = {
  nameIcon: 'CheckIcon',
  colorIcon: 'icon-success',
  className: 'text-list',
  data: listText
}


