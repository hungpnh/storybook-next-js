import React from 'react'
import Icon from '../Icon'


const TextList = ({ nameIcon, colorIcon,  data, className }) => {
  return (
    <ul>
        {data.map((feature, featureIdx) =>
          <li key={featureIdx} className="flex">
          <Icon
            className={colorIcon}
            name={nameIcon}
            optionsType="outline"
          />
          <span className={className}>{feature}</span>
        </li>
      )}
    </ul>
  )
}

export default TextList;