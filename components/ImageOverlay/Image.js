import React from 'react'

const Image = ({ src = "", alt = "", width, overlayText, className }) => {
  return <div className={className ? className : "image"} style={{ width: width }}>
    <img loading="lazy" src={src} alt={alt} />
    <div className="overlay">
      <p>{overlayText}</p>
    </div>
  </div>
}

export default Image;