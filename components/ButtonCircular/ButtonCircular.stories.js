import React from 'react';
import ButtonCircular from './ButtonCircular';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonCircular,
  argTypes: { onClick: { action: 'clicked' } },
}

const Template = (args) => <ButtonCircular {...args}/>

export const Circular = Template.bind({})
Circular.args = {
  className: 'button-circular',
  disabled: false,
  buttonStyle: {},
  iconName: 'PlusIcon',
}

