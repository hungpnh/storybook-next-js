import React from 'react';
import Heading4 from './Heading4';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading4,
}

const Template = (args) => <Heading4 {...args} />

export const H4 = Template.bind({})
H4.args = {
  message: 'h4 display 4',
  className: ""
}

