import React from 'react'

const Heading4 = ({ message, className }) => {
    return <h1 className={className ? className : "heading4"}>
        {message}
    </h1>
}

export default Heading4;