import React from 'react';
import AlertWarning from './AlertWarning';

export default {
    title: 'MangoAds/Atoms/Alert',
    component: AlertWarning,
}

const Template = (args) => <AlertWarning {...args} />

export const Warning = Template.bind({})
Warning.args = {
    title: "Title Alert",
    open: true,
    message: "message alert",
    className: "",
}

