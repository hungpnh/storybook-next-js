import { useEffect, useState } from "react"

const AlertWarning = ({ open = true, message = "message alert", className, title }) => {
    const [show, setShow] = useState("")
    useEffect(() => {
        setShow(open ? "" : "hide")
    }, [open])
    return (

        <div className={`${(className ? className : "alert alert-warning")} ${show}  `} role="alert">
            <svg xmlns="http://www.w3.org/2000/svg" className="icon" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>
            <div className="title-wrapper">
                <p className="title">
                    {title}
                </p>
                <span >  {message}</span>
            </div>
            <span className="icon-wrapper">
                <svg onClick={() => {
                    setShow("hide")
                }} className="icon-close" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" /></svg>
            </span>
        </div>

    )
}

export default AlertWarning
