import React from 'react'

const Heading6 = ({ message, className }) => {
    return <h1 className={className ? className : "heading6"}>
        {message}
    </h1>
}

export default Heading6;