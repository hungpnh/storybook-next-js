import React from 'react';
import Heading6 from './Heading6';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading6,
}

const Template = (args) => <Heading6 {...args} />

export const H6 = Template.bind({})
H6.args = {
  message: 'h6 display 6',
  className: ""
}

