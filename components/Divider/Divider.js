import React from 'react'

const Divider = ({ className, dividerStyle }) => {
  return (
    <div className={className?.length != 0 ? className : 'divider-primary'} style={dividerStyle}></div>
  )
}

export default Divider;