import React from 'react';
import Badge from './Badge';

export default {
  title: 'MangoAds/Atoms/Badge',
  component: Badge,
}

const Template = (args) => <Badge {...args} />

export const Warning = Template.bind({})
Warning.args = {
  message: "0",
  className: ""
}


