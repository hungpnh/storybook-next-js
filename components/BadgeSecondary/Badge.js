import React from 'react'

const Badge = ({ message = "0", className }) => {

  return (

    <div >
      <span className={className ? className : "badge-secondary"}>{message}</span>
    </div>

  )

}

export default Badge;