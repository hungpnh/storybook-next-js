import React from 'react'

const Color = ({ color, className }) => {
  if (className) color = ""
  return <div style={{ backgroundColor: color }} className={className ? className : "color"}>

  </div>
}

export default Color;