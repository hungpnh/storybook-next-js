import React from 'react';
import CircleIconDanger from './CircleIconDanger';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/CircleIcon',
  component: CircleIconDanger,
  argTypes: {
    optionsType: {
      control: {
        type: 'inline-radio',
        options: ['solid', 'outline'],
      },
      table: { category: 'Options' },
      defaultValue: "outline"
    },
    colorIcon: {
      control: {
        type: 'select',
        options: ['icon-primary', 'icon-warning', 'icon-danger', 'icon-secondary', 'icon-success', 'icon-white', 'icon-black']
      },
      defaultValue: 'icon-white'
    },
    className: {
      control: {
        type: 'select',
        options: ['circle-icon-primary', 'circle-icon-secondary', 'circle-icon-success', 'circle-icon-danger', 'circle-icon-warning', 'circle-icon-white', 'circle-icon-black'],
      },
      defaultValue: "circle-icon-primary"
    },
    onClick: { action: 'clicked' },
  }
}

const Template = (args) => <CircleIconDanger {...args} />

export const Danger = Template.bind({})
Danger.args = {
  className: 'circle-icon-danger',
  nameIcon: 'HomeIcon',
}

