import React, { useState } from 'react'

const QuantitySelector = ({ disabledIncrease, disabledDecrease, disabledLabel, className }) => {
  const [quantity, setQuantity] = useState(1)
  const handleClickDecrease = () => {

    setQuantity(+quantity - 1)
  }
  const handleClickIncrease = () => {

    setQuantity(+quantity + 1);
  }
  const handleOnChange = (e) => {

    if (!isNaN(e.target.value)) {
      setQuantity(e.target.value)
    }
  }
  return <div className={className ? className : "quantity"}>
    <button disabled={disabledDecrease} className="btn" onClick={handleClickDecrease}>-</button>

    <input disabled={disabledLabel} size="1" onChange={handleOnChange} value={quantity} />

    <button disabled={disabledIncrease} className="btn" onClick={handleClickIncrease}>+</button>
  </div>
}

export default QuantitySelector;