import React from 'react';
import Label from './Label';
import { action } from '@storybook/addon-actions';

export default {
  title: 'MangoAds/Atoms/Text',
  component: Label,
}

const Template = (args) => <Label {...args} />

export const Primary = Template.bind({})
Primary.args = {
  className: 'label-primary',
  htmlFor: ''
}


