import React from 'react';
import CircleIconSecondary from './CircleIconSecondary';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/CircleIcon',
  component: CircleIconSecondary,
  argTypes: {
    optionsType: {
      control: {
        type: 'inline-radio',
        options: ['solid', 'outline'],
      },
      table: { category: 'Options' },
      defaultValue: "outline"
    },
    onClick: { action: 'clicked' },
  }
}

const Template = (args) => <CircleIconSecondary {...args}/>

export const Secondary = Template.bind({})
Secondary.args = {
    className: 'circle-icon-secondary',
    nameIcon: 'HomeIcon',
}

