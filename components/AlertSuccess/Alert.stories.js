import React from 'react';
import AlertSuccess from './AlertSuccess';

export default {
    title: 'MangoAds/Atoms/Alert',
    component: AlertSuccess,
}

const Template = (args) => <AlertSuccess {...args} />

export const Success = Template.bind({})
Success.args = {
    title: "Title Alert",
    open: true,
    message: "message alert",
    className: "",

}

