import React from 'react';
import LoaderText from './LoaderText';

export default {
  title: 'MangoAds/Atoms/Loader',
  component: LoaderText,
  argTypes: {
    className: {
      control: {
        type: 'select',
        options: ['loader-spin', 'loader-ping']
      },
      defaultValue: 'loader-ping'
    }
  }
}

const Template = (args) => <LoaderText {...args}/>

export const Text = Template.bind({})
Text.args = {
    message: 'Loading...',
    loaderStyle: {}
}
