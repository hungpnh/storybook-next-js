import React from 'react';
import Badge from './Badge';

export default {
  title: 'MangoAds/Atoms/Badge',
  component: Badge,
}

const Template = (args) => <Badge {...args} />

export const Success = Template.bind({})
Success.args = {
  message: "0",
  className: ""
}


