import React from 'react';
import Input from './Input';

export default {
  title: 'MangoAds/Atoms/Input',
  component: Input,
  argTypes: {
    type: {
      control: {
        type: 'select',
        options: ['text', 'number', 'date', 'time', 'checkbox', 'radio', 'submit', 'file']
      },
      defaultValue: 'text'
    }
  }
}

const Template = (args) => <Input {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    className: 'input-primary',
    placeholder: 'Please enter something',
    name: 'email',
    disabled: false,
    inputStyle: {}
}

